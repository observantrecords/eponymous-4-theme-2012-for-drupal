<?php
$albums = array();
if (class_exists('OR_Albums')) {
	$album_info = new OR_Albums();
	$albums = $album_info->get_albums('eponymous-4', array(
		'filters' => array(
			'album_format_id' => 1,
		),
	) );

	$singles = $album_info->get_albums('eponymous-4', array(
		'filters' => array(
			'album_format_id' => 2,
		),
	) );

	$eps = $album_info->get_albums('eponymous-4', array(
		'filters' => array(
			'album_format_id' => 3,
		),
	) );

	$compilations = $album_info->get_albums('eponymous-4', array(
		'filters' => array(
			'album_format_id' => 4,
		),
	) );
	
	$node_ids = db_query('select * from {node} where type = :type', array(':type' => 'album'))->fetchAllAssoc('nid', PDO::FETCH_ASSOC);
	$album_aliases = array();

	foreach ($node_ids as $node_id => $node_info) {
		$node = node_load($node_id);
		if (!empty($node->field_album_alias)) {
			$album_aliases[] = $node->field_album_alias[$node->language][0]['value'];
		}
	}

}
?>

<?php if (!empty($albums)):?>
	<h3>Albums</h3>

	<ul class="album-list">
	<?php foreach ($albums as $album):
		$cover_url_base = OBSERVANTRECORDS_CDN_BASE_URI . '/artists/' . (!empty($artist_alias) ? $artist_alias : 'eponymous-4') . '/albums/' . $album['album_alias'] . '/' . strtolower($album['release_catalog_num']) . '/images';
	?>
		<?php if (false !== array_search($album['album_alias'], $album_aliases)): ?>
	<li>
		<a href="/music/<?php echo $album['album_alias'];?>"><img src="<?php echo $cover_url_base . '/cover_front_medium.jpg' ;?>" alt="[<?php echo $album['album_title'] ?>]" title="[<?php echo $album['album_title']; ?>]" width="200" /></a>
	</li>
		<?php endif; ?>
	<?php endforeach; ?>
</ul>
<?php endif; ?>

<?php if (!empty($eps)):?>
	<h3>EPs</h3>

	<ul class="album-list">
		<?php foreach ($eps as $ep):
			$cover_url_base = OBSERVANTRECORDS_CDN_BASE_URI . '/artists/' . (!empty($artist_alias) ? $artist_alias : 'eponymous-4') . '/albums/' . $ep['album_alias'] . '/' . strtolower($ep['release_catalog_num']) . '/images';
			?>
			<?php if (false !== array_search($ep['album_alias'], $album_aliases)): ?>
			<li>
				<a href="/music/<?php echo $ep['album_alias'];?>"><img src="<?php echo $cover_url_base . '/cover_front_medium.jpg' ;?>" alt="[<?php echo $ep['album_title'] ?>]" title="[<?php echo $ep['album_title']; ?>]" width="200" /></a>
			</li>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>

<?php if (!empty($singles)):?>
	<h3>Singles</h3>

	<ul class="album-list">
		<?php foreach ($singles as $single):
			$cover_url_base = OBSERVANTRECORDS_CDN_BASE_URI . '/artists/' . (!empty($artist_alias) ? $artist_alias : 'eponymous-4') . '/albums/' . $single['album_alias'] . '/' . strtolower($single['release_catalog_num']) . '/images';
			?>
			<?php if (false !== array_search($single['album_alias'], $album_aliases)): ?>
			<li>
				<a href="/music/<?php echo $single['album_alias'];?>"><img src="<?php echo $cover_url_base . '/cover_front_medium.jpg' ;?>" alt="[<?php echo $single['album_title'] ?>]" title="[<?php echo $single['album_title']; ?>]" width="200" /></a>
			</li>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>

<?php if (!empty($compilations)):?>
	<h3>Compilations</h3>

	<ul class="album-list">
		<?php foreach ($compilations as $compilation):
			$cover_url_base = OBSERVANTRECORDS_CDN_BASE_URI . '/artists/' . (!empty($artist_alias) ? $artist_alias : 'eponymous-4') . '/albums/' . $compilation['album_alias'] . '/' . strtolower($compilation['release_catalog_num']) . '/images';
			?>
			<?php if (false !== array_search($compilation['album_alias'], $album_aliases)): ?>
			<li>
				<a href="/music/<?php echo $compilation['album_alias'];?>"><img src="<?php echo $cover_url_base . '/cover_front_medium.jpg' ;?>" alt="[<?php echo $compilation['album_title'] ?>]" title="[<?php echo $compilation['album_title']; ?>]" width="200" /></a>
			</li>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
